package com.example.bluetooth;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    /**
     * Variables FINAL
     */
    private static final String DATE_FORMAT = "d-M-yyyy";
    /**
        private static final String URL_TEST = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/5-7-2019/students/201212651";
    */
    private static final String URL = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/";

    private static final String TAG = "MainActivity";
    private static final String TAGFECHA = "FormatDate";
    private static final String STUDENTS_PATH = "/students";
    private static final String ID_STUDENTS = "/201327471";

    private static final int ENABLE_BT = 0;
    private static final int DISCOVERY_BT = 1;

    /**
     * Butoones, texto, que van ha estar en la interfaz del app movil
     */
    private TextView mStatusBlueTv, mStatusBlueMAC, mAssistanceMov;
    private ImageView mBlueTv;
    private Button mOnBtn, mOffBtn, mDiscoverBtn, mTryAgain;

    private BluetoothAdapter mBlueAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStatusBlueTv = findViewById(R.id.statusBluetoothTv);
        mStatusBlueMAC = findViewById(R.id.statusBluetoothMAC);
        mAssistanceMov = findViewById(R.id.statusAssistanceMov);
        mBlueTv = findViewById(R.id.bluetoothTv);
        mOnBtn = findViewById(R.id.onBtn);
        mOffBtn = findViewById(R.id.offBtn);
        mDiscoverBtn = findViewById(R.id.discoverableBtn);
        mTryAgain = findViewById(R.id.tryagain);
        /*
          Obtiene la MAC del dispositivo
         */
        this.getMAC();

        /*
          Obtiene si el bluetooth esta disponible
         */
        if (getBluetooth()) return;

        /*
          Pinta la imagen dependiendo de si el bluetooth esta prendido
         */
        this.paintBluetooth();

        /*
            Obtiene si estas inscrito o no en firestore
         */
        this.getAttendance();

        /*
          Prende el bluetooth
         */
        this.getBluetoothOn();

        /*
          Apaga el bluetooth
         */
        this.getBluetoothOff();

        /*
          Hace que el dispositivo sea visible
         */
        this.getDiscoveryOn();

        /*
            Busca de nuevo en la base de datos
         */
        mTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAttendance();
            }
        });
    }

    private void getDiscoveryOn() {
        mDiscoverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mBlueAdapter.isDiscovering()){
                    showMessage("Making Your Device Discoverable");
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                    startActivityForResult(intent, DISCOVERY_BT);
                    mBlueTv.setImageResource(R.drawable.ic_action_bluetooth);
                }else{
                    showMessage("Bluetooth is already Discoverable");
                }
            }
        });
    }

    private void getBluetoothOff() {
        mOffBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBlueAdapter.isEnabled()){
                    mBlueAdapter.disable();
                    showMessage("Turning Bluetooth Off");
                    mBlueTv.setImageResource(R.drawable.ic_action_off);
                }else{
                    showMessage("Bluetooth is already off");
                }
            }
        });
    }

    private void getBluetoothOn() {
        mOnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mBlueAdapter.isEnabled()){
                    showMessage("Turning on Bluetooth");
                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intent, ENABLE_BT);
                }else{
                    showMessage("Bluetooth is already on");
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void getAttendance() {
    /*
      Crea el formato de la fecha del dia
     */
        String dateFormat = getDate();

        /*
          Genera la URL de la base de datos
         */
        String urlCompleted = URL + dateFormat + STUDENTS_PATH + ID_STUDENTS;
        Log.w(TAG, urlCompleted);

        /*
          Busca el ID de estudiante con la fecha establecida
         */
        try {
            RequestQueue que = Volley.newRequestQueue(this);
            JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, urlCompleted, null, new Response.Listener<JSONObject>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(JSONObject response) {
                    Log.w(TAG, "FOUND");
                    Log.w(TAG, response.toString());
                    mAssistanceMov.setText("You are Registered");
                    mTryAgain.setText("YEI");
                }
            }, new Response.ErrorListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onErrorResponse(VolleyError e) {
                    Log.w(TAG, "NO ESTA INSCRITO");
                    Log.w(TAG, e.toString());
                    mAssistanceMov.setError(e.toString());
                    mAssistanceMov.setText("Not Registered");
                    mTryAgain.setText("TELL TO MARIO");
                }
            });
            que.add(getRequest);
        } catch(Exception e){
            mAssistanceMov.setError(e.toString());
            mAssistanceMov.setText("ERROR IN EXCEPTION");
            Log.w(TAG, e.getMessage());
        }
    }

    private String getDate() {
        String dateFormat = "";
        try{
            @SuppressLint("SimpleDateFormat") @SuppressWarnings("deprecation")
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -1);
            dateFormat = simpleDateFormat.format(calendar.getTime());
            Log.w(TAGFECHA, dateFormat);
        }catch(Exception e){
            Log.w(TAGFECHA, e.toString());
        }
        return dateFormat;
    }

    private void paintBluetooth() {
        if(mBlueAdapter.isEnabled()){
            mBlueTv.setImageResource(R.drawable.ic_action_bluetooth);
        }else{
            mBlueTv.setImageResource(R.drawable.ic_action_off);
        }
    }

    @SuppressLint("SetTextI18n")
    private boolean getBluetooth() {
        mBlueAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBlueAdapter == null){
            mStatusBlueTv.setText("Bluetooth is not available");
            mBlueTv.setImageResource(R.drawable.ic_action_warning);
            return true;
        }else{
            mStatusBlueTv.setText("Bluetooth is available");
        }
        return false;
    }

    @SuppressLint("SetTextI18n")
    private void getMAC() {
        String macAddress = android.provider.Settings.Secure.getString(getContentResolver(), "bluetooth_address");
        if(macAddress != null){
            mStatusBlueMAC.setText(macAddress);
        }else{
            mStatusBlueMAC.setText("It is not possible to find the Bluetooth MAC");
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                mBlueTv.setImageResource(R.drawable.ic_action_bluetooth);
                showMessage("Bluetooth is on");
            } else {
                showMessage("couldn't on bluetooth");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showMessage(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
